<?php

Route::group(['middleware' => 'web'], function() { // middleware web required for error share
	Route::prefix(Cradle::config('project.doorbell.admin'))->group(function() {
		Cradle::routesBread('oauth_client','WCPassport\Controllers\admin\bread\OauthClientBreadController');
	});
});