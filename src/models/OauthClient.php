<?php
namespace WCPassport\models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AppCradleModel;

class OauthClient extends AppCradleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
}
