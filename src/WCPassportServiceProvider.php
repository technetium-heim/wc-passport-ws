<?php

namespace WCPassport;

use Illuminate\Support\ServiceProvider;

class WCPassportServiceProvider extends ServiceProvider
{
    public function boot()
    {    
    	$this->loadRoutesFrom(__DIR__.'/routes.php');  
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    public function register()
    { 
        //  
    }
}

